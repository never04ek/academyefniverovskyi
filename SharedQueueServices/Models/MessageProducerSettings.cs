using RabbitMQ.Client;

namespace SharedQueueServices.Models
{
    public class MessageProducerSettings
    {
        public IModel Channel { get; set; }
        public PublicationAddress PublicationAdderess { get; set; }
    }
}