using RabbitMQ.Client;

namespace SharedQueueServices.Models
{
    public class MessageConsumerSettings
    {
        public bool SequentialFetch { get; set; }
        public bool AutoAcknowledge { get; set; }
        public IModel Channel { get; set; }
        public string QueueName { get; set; }
    }
}