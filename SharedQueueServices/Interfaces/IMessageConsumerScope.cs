using System;

namespace SharedQueueServices.Interfaces
{
    
    public abstract class IMessageConsumerScope : IDisposable
    {
        public IMessageConsumer MessageConsumer;
       
        public void Dispose()
        {
            
        }
    }
}