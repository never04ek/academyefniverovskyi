using System;
using System.IO;
using System.Text;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using SharedQueueServices.Interfaces;
using SharedQueueServices.Models;

namespace Worker
{
    public class MessageService : IDisposable
    {
        private readonly IMessageProducerScope _messageProducerScope;
        private readonly IMessageConsumerScope _messageConsumerScope;

        public MessageService(IMessageConsumerScopeFactory messageConsumerScopeFactory,
            IMessageProducerScopeFactory messageProducerScopeFactory)
        {
            _messageConsumerScope = messageConsumerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "ServerExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "SendValueQueue",
                RoutingKey = "*.queue.#"
            });

            _messageConsumerScope.MessageConsumer.Received += MessageReceived;

            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "ClientExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "SendResponseQueue",
                RoutingKey = "response"
            });
        }

        public void Run()
        {
            Console.WriteLine("Service run");
        }


        private void MessageReceived(object sender, BasicDeliverEventArgs e)
        {
            var processed = false;
            try
            {
                var value = Encoding.UTF8.GetString(e.Body);
                File.WriteAllText("CommandLog.txt", value);
                Console.WriteLine(value);
                SendSuccesfulState(value);
                processed = true;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                File.WriteAllText("CommandLog.txt", exception.Message);
                processed = false;
            }
            finally
            {
                _messageConsumerScope.MessageConsumer.SetAcknoledge(e.DeliveryTag, processed);
            }
        }

        private void SendSuccesfulState(string value)
        {
            _messageProducerScope.MessageProducer.Send(value);
        }

        public void Dispose()
        {
            _messageConsumerScope.Dispose();
            _messageProducerScope.Dispose();
        }
    }
}