﻿using System;
using BusinessLogic.Hubs;
using BusinessLogic.Services;
using DataAccess;
using DataAccess.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Models;
using RabbitMQ.Client;
using SharedQueueServices.Interfaces;
using SharedQueueServices.QueueServices;
using ConnectionFactory = RabbitMQ.Client.ConnectionFactory;

//using ConnectionFactory = SharedQueueServices.QueueServices.ConnectionFactory;

namespace BusinessLogic
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRouting(options => options.LowercaseUrls = true);
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddDbContext<AcademyDbContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("DefaultConnection")));

            services.AddSingleton<ProjectsService>();
            services.AddSingleton<TasksService>();
            services.AddSingleton<TeamsService>();
            services.AddSingleton<UsersService>();
            services.AddSingleton<LinqService>();

            services.AddScoped<IQueueService, QueueService>();
            services.AddScoped<IMessageQueue, MessageQueue>();
            services.AddSingleton<IConnectionFactory>(x =>
                new ConnectionFactory {Uri = new Uri(Configuration.GetSection("Rabbit").Value)});


            services.AddScoped<IMessageProducer, MessageProducer>();
            services.AddScoped<IMessageProducerScope, MessageProducerScope>();
            services.AddSingleton<IMessageProducerScopeFactory, MessageProducerScopeFactory>();

            services.AddScoped<IMessageConsumer, MessageConsumer>();
            services.AddScoped<IMessageConsumerScope, MessageConsumerScope>();
            services.AddSingleton<IMessageConsumerScopeFactory, MessageConsumerScopeFactory>();

            services.AddSingleton<IRepository<Project>, ProjectRepository>();
            services.AddSingleton<IRepository<Tasks>, TasksRepository>();
            services.AddSingleton<IRepository<Team>, TeamRepository>();
            services.AddSingleton<IRepository<User>, UserRepository>();


            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseSignalR(routes => { routes.MapHub<MessageHub>("/hub"); });

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}