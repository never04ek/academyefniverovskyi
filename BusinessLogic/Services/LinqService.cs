using System;
using System.Collections.Generic;
using System.Linq;
using Models;

namespace BusinessLogic.Services
{
    public class LinqService
    {
        public LinqService()
        {
        }
        
        #region Task1

        public  Dictionary<Project, int> GetAmountOfUserTasksInEachProject(int id)
        {
            var projects = ProjectsService._projectRepository.GetAll();
            return TasksService._tasksRepository.GetAll()
                .Where(task => task.PerformerId == id)
                .GroupBy(task => task.ProjectId)
                .ToDictionary(group => projects[group.Key - 1], group => group.Count());
        }

        #endregion
        
        #region Task2

        public List<Tasks> GetUserTasks45Name(int id)
        {
            return TasksService._tasksRepository.GetAll()
                .Where(task => task.PerformerId == id)
                .Where(task => task.Name.Length < 45)
                .ToList();
        }

        #endregion

        #region Task3

        public List<(int, string)> GetThisYearTasksOfUser(int id)
        {
            return TasksService._tasksRepository.GetAll()
                .Where(task => task.PerformerId == id)
                .Where(task => task.FinishedAt.Year == 2019)
                .Select(task => (task.Id, task.Name))
                .ToList();
        }

        #endregion

        #region Task4

        public List<(int?, string, List<User>)> GetTeamsAndUsers()
        {
            //Displays nothing, because all teams have participants under 12 years old.
            var users = UsersService._userRepository.GetAll();
            var teams = TeamsService._teamRepository.GetAll();
            return users
                .Where(user => user.TeamId != null)
                .GroupBy(user => user.TeamId)
                .Where(team =>
                    team.All(user => DateTime.Now.Year - user.Birthday.Year > 12))
                .Select(team => (team.Key, teams[(int) team.Key - 1].Name,
                    team.OrderByDescending(user => user.TimeOfRegistration).ToList()))
                .ToList();
        }

        #endregion

        #region Task5

        public List<(User, List<Tasks>)> GetUserAndTasks()
        {
            var users = UsersService._userRepository.GetAll();
            return TasksService._tasksRepository.GetAll()
                .GroupBy(task => task.PerformerId)
                .Select(userT => (users[userT.Key - 1], userT.OrderByDescending(task => task.Name.Length).ToList()))
                .OrderBy(user => user.Item1.FirstName)
                .ToList();
        }

        #endregion

        #region Task6

        public (User, Project, int, int, Tasks)? GetSpecUserStruct(int id)
        {
            var user = UsersService._userRepository.Get(id);
            var projects = ProjectsService._projectRepository.GetAll();
            var tasks = TasksService._tasksRepository.GetAll();
            var res = projects
                .Where(project => project.AuthorId == user.Id)
                .Where(project =>
                    project.CreatedAt == projects
                        .Where(projectmin => projectmin.AuthorId == user.Id)
                        .Max(projectmin => projectmin.CreatedAt))
                .Select(project => (user,
                        project,
                        tasks.Count(task => project.Id == task.ProjectId),
                        tasks.Where(task => task.PerformerId == user.Id)
                            .Count(task => task.State == TaskState.Finished || task.State == TaskState.Canceled),
                        tasks.Where(task => task.PerformerId == user.Id)
                            .OrderBy(task => task.FinishedAt - task.CreatedAt).Last()
                    )).FirstOrDefault();


            return res;
        }

        #endregion

        #region Task7

        public (Project, Tasks, Tasks, int?)? GetSpecProjectStruct(int id)
        {
            var users = UsersService._userRepository.GetAll();
            var projects = ProjectsService._projectRepository.GetAll();
            var tasks = TasksService._tasksRepository.GetAll();
            var res = tasks.Where(task => task.ProjectId == id)
                .Where(task =>
                    task.DescriptionTask.Length == tasks.Where(taskd => taskd.ProjectId == id)
                        .Max(taskd => taskd.DescriptionTask.Length)).Select(task => (projects[id - 1], task,
                        tasks
                            .Where(taskn =>
                                taskn.ProjectId == id).FirstOrDefault(taskn =>
                                taskn.Name.Length == tasks.Where(tasknn => tasknn.ProjectId == id)
                                    .Min(tasknn => tasknn.Name.Length)),
                        users.Count(user => user.TeamId == projects[id - 1].TeamId &&
                                            (projects[id - 1].Description.Length > 25 ||
                                             tasks.Count(taskc => taskc.ProjectId == id) < 3)) == 0
                            ? null
                            : (int?) users.Count(user =>
                                user.TeamId == projects[id - 1].TeamId)
                    )).FirstOrDefault();

            return res;
        }

        #endregion
    }
}