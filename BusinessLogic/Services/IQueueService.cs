using RabbitMQ.Client.Events;

namespace BusinessLogic.Services
{
    public interface IQueueService
    {
        void GetValue(object sender, BasicDeliverEventArgs e);
        bool PostValue(string value);
    }
}