using System.Collections.Generic;
using System.Linq;
using DataAccess.Repository;
using Models;

namespace BusinessLogic.Services
{
    public class UsersService
    {
        public static IRepository<User> _userRepository;

        public UsersService(UserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public List<User> GetUsers()
        {
            return _userRepository.GetAll().ToList();
        }

        public User GetUser(int id)
        {
            return _userRepository.Get(id);
        }

        public void AddUser(User user)
        {
            _userRepository.Add(user);
        }

        public void UpdateUser(int id, User user)
        {
            user.Id = id;
            _userRepository.Update(user);
        }

        public void DeleteUser(int id)
        {
            _userRepository.Delete(_userRepository.GetAll().FirstOrDefault(pr => pr.Id==id));
        }
    }
}