using System.Collections.Generic;
using System.Linq;
using DataAccess;
using DataAccess.Repository;
using Models;

namespace BusinessLogic.Services
{
    public class ProjectsService
    {
        public static IRepository<Project> _projectRepository;

        public ProjectsService(ProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }

        public IEnumerable<Project> GetProjects()
        {
            return _projectRepository.GetAll();
        }

        public Project GetProject(int id)
        {
            return _projectRepository.Get(id);
        }

        public void AddProject(Project project)
        {
            _projectRepository.Add(project);
        }

        public void UpdateProject(int id, Project project)
        {
            project.Id = id;
            _projectRepository.Update(project);
        }

        public void DeleteProject(int id)
        {
            _projectRepository.Delete(_projectRepository.GetAll().FirstOrDefault(pr => pr.Id==id));
        }
    }
}