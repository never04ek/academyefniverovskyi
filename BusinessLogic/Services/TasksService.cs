using System.Collections.Generic;
using System.Linq;
using DataAccess.Repository;
using Models;

namespace BusinessLogic.Services
{
    public class TasksService
    {
        public static TasksRepository _tasksRepository;

        public TasksService(TasksRepository tasksRepository)
        {
            _tasksRepository = tasksRepository;
        }

        public List<Tasks> GetTasks()
        {
            return _tasksRepository.GetAll().ToList();
        }

        public Tasks GetTask(int id)
        {
            return _tasksRepository.Get(id);
        }

        public void AddTask(Tasks tasks)
        {
            _tasksRepository.Add(tasks);
        }

        public void UpdateTask(int id, Tasks task)
        {
            task.Id = id;
            _tasksRepository.Update(task);
        }

        public void DeleteTask(int id)
        {
            _tasksRepository.Delete(_tasksRepository.GetAll().FirstOrDefault(pr => pr.Id==id));
        }
    }
}