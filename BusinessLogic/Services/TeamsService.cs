using System.Collections.Generic;
using System.Linq;
using DataAccess.Repository;
using Models;

namespace BusinessLogic.Services
{
    public class TeamsService
    {
        public static IRepository<Team> _teamRepository;

        public TeamsService(TeamRepository teamRepository)
        {
            _teamRepository = teamRepository;
        }

        public List<Team> GetTeams()
        {
            return _teamRepository.GetAll().ToList();
        }

        public Team GetTeam(int id)
        {
            return _teamRepository.Get(id);
        }

        public void AddTeam(Team team)
        {
            _teamRepository.Add(team);
        }

        public void UpdateTeam(int id, Team team)
        {
            team.Id = id;
            _teamRepository.Update(team);
        }

        public void DeleteTeam(int id)
        {
            _teamRepository.Delete(_teamRepository.GetAll().FirstOrDefault(pr => pr.Id==id));
        }
    }
}