using System.Collections.Generic;
using BusinessLogic.Services;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace BusinessLogic.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly TeamsService _teamsService;

        public TeamsController(TeamsService teamsService)
        {
            _teamsService = teamsService;
        }

        // GET api/teams
        [HttpGet]
        public ActionResult<IEnumerable<Team>> Get()
        {
            return Ok(_teamsService.GetTeams());
        }

        // GET api/teams/5
        [HttpGet("{id}")]
        public ActionResult<Team> Get(int id)
        {
            return Ok(_teamsService.GetTeam(id));
        }

        // POST api/teams
        [HttpPost]
        public void Post([FromBody] Team value)
        {
            _teamsService.AddTeam(value);
        }

        // PUT api/teams/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Team value)
        {
            _teamsService.UpdateTeam(id, value);
        }

        // DELETE api/teams/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _teamsService.DeleteTeam(id);
        }
    }
}