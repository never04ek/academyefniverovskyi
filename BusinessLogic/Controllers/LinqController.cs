using BusinessLogic.Services;
using Microsoft.AspNetCore.Mvc;

namespace BusinessLogic.Controllers

{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqController : ControllerBase
    {
        private readonly LinqService _linqService;

        public LinqController(LinqService linqService)
        {
            _linqService = linqService;
        }


        // GET api/linq/task1?id=5
        [HttpGet("task1")]
        public ActionResult GetTask1(int id)
        {
            return Ok(_linqService.GetAmountOfUserTasksInEachProject(id));
        }

        // GET api/linq/task2?id=5
        [HttpGet("task2")]
        public ActionResult GetTask2(int id)
        {
            return Ok(_linqService.GetUserTasks45Name(id));
        }

        // GET api/linq/task3?id=5
        [HttpGet("task3")]
        public ActionResult GetTask3(int id)
        {
            return Ok(_linqService.GetThisYearTasksOfUser(id));
        }

        // GET api/linq/task4
        [HttpGet("task4")]
        public ActionResult GetTask4()
        {
            return Ok(_linqService.GetTeamsAndUsers());
        }

        // GET api/linq/task5
        [HttpGet("task5")]
        public ActionResult GetTask5()
        {
            return Ok(_linqService.GetUserAndTasks());
        }

        // GET api/linq/task6?id=5
        [HttpGet("task6")]
        public ActionResult GetTask6(int id)
        {
            return Ok(_linqService.GetSpecUserStruct(id));
        }

        // GET api/linq/task7?id=5
        [HttpGet("task7")]
        public ActionResult GetTask7(int id)
        {
            return Ok(_linqService.GetSpecProjectStruct(id));
        }
    }
}