using System;
using System.Collections.Generic;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Models;

namespace DataAccess
{
    public partial class AcademyDbContext : DbContext
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = "Westley",
                    LastName = "Conn",
                    Email = "Westley_Conn@yahoo.com",
                    Birthday = Convert.ToDateTime("2002-04-28T04:50:58.3713304+00:00"),
                    TimeOfRegistration = Convert.ToDateTime("2019-06-14T01:55:00.7355536+00:00"),
                    TeamId = 7
                },
                new User
                {
                    Id = 2,
                    FirstName = "Adolphus",
                    LastName = "Zulauf",
                    Email = "Adolphus_Zulauf@yahoo.com",
                    Birthday = Convert.ToDateTime("2006-12-04T08:12:42.2387444+00:00"),
                    TimeOfRegistration = Convert.ToDateTime("2019-06-13T19:29:27.1143526+00:00"),
                    TeamId = null
                }
            };

            var projects = new List<Project>
            {
                new Project
                {
                    Id = 1,
                    Name = "Vero fugiat temporibus et praesentium.",
                    Description =
                        "Labore tempora quo ex voluptatem ut distinctio non.\nQuibusdam sequi molestiae.\nSit corrupti aut aut.\nReiciendis voluptatem libero fugit ut saepe.\nOmnis omnis incidunt perferendis ex autem blanditiis reiciendis veritatis et.\nUt voluptates rem eaque quia esse.",
                    CreatedAt = Convert.ToDateTime("2019-07-12T00:20:40.99888+00:00"),
                    Deadline = Convert.ToDateTime("2020-01-07T20:48:40.709841+00:00"),
                    AuthorId = 9,
                    TeamId = 5
                },
                new Project
                {
                    Id = 2,
                    Name = "Quos minima quis doloremque voluptates.",
                    Description = "Mollitia beatae quia quia rerum esse.\nEos et adipisci libero alias cumque ducimus",
                    CreatedAt = Convert.ToDateTime("2019-07-11T17:04:23.349188+00:00"),
                    Deadline = Convert.ToDateTime("2019-08-22T18:56:31.5473426+00:00"),
                    AuthorId = 39,
                    TeamId = 1
                },
            };

            var tasks = new List<Tasks>
            {
                new Tasks
                {
                    Id = 1,
                    Name = "Natus officiis nobis molestiae.",
                    DescriptionTask = "Iste doloribus facere.\nTempora id voluptatibus voluptas vel.",
                    CreatedAt = Convert.ToDateTime("2019-07-12T02:21:27.8240952+00:00"),
                    FinishedAt = Convert.ToDateTime("2020-03-23T21:48:17.2339748+00:00"),
                    State = (TaskState) 2,
                    ProjectId = 14,
                    PerformerId = 20
                },
                new Tasks
                {
                    Id = 2,
                    Name = "Omnis ab et eum odit qui quasi consequuntur expedita.",
                    DescriptionTask = "Esse recusandae quia velit eos distinctio quasi ut.\nEius eaque est incidunt.",
                    CreatedAt = Convert.ToDateTime("2019-07-12T07:19:20.571115+00:00"),
                    FinishedAt = Convert.ToDateTime("2020-04-19T12:30:49.6776879+00:00"),
                    State = (TaskState) 2,
                    ProjectId = 46,
                    PerformerId = 37
                }
            };

            var teams = new List<Team>
            {
                new Team
                {
                    Id = 1,
                    Name = "dolor",
                    CreatedAt = Convert.ToDateTime("2019-07-11T17:01:35.5381535+00:00")
                },
                new Team
                {
                    Id = 2,
                    Name = "aut",
                    CreatedAt = Convert.ToDateTime("2019-07-11T22:54:37.6268491+00:00")
                }
            };
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Tasks>().HasData(tasks);
            modelBuilder.Entity<Team>().HasData(teams);
            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=academy.db");
        }

        public DbSet<Project> Projects { get; set; }
        public DbSet<Tasks> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }
    }
}