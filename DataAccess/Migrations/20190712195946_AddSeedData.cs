﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class AddSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Projects",
                nullable: true);

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, 9, new DateTime(2019, 7, 12, 3, 20, 40, 998, DateTimeKind.Local).AddTicks(8800), new DateTime(2020, 1, 7, 22, 48, 40, 709, DateTimeKind.Local).AddTicks(8410), @"Labore tempora quo ex voluptatem ut distinctio non.
Quibusdam sequi molestiae.
Sit corrupti aut aut.
Reiciendis voluptatem libero fugit ut saepe.
Omnis omnis incidunt perferendis ex autem blanditiis reiciendis veritatis et.
Ut voluptates rem eaque quia esse.", "Vero fugiat temporibus et praesentium.", 5 });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 2, 39, new DateTime(2019, 7, 11, 20, 4, 23, 349, DateTimeKind.Local).AddTicks(1880), new DateTime(2019, 8, 22, 21, 56, 31, 547, DateTimeKind.Local).AddTicks(3426), @"Mollitia beatae quia quia rerum esse.
Eos et adipisci libero alias cumque ducimus", "Quos minima quis doloremque voluptates.", 1 });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "DescriptionTask", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { 1, new DateTime(2019, 7, 12, 5, 21, 27, 824, DateTimeKind.Local).AddTicks(952), @"Iste doloribus facere.
Tempora id voluptatibus voluptas vel.", new DateTime(2020, 3, 23, 23, 48, 17, 233, DateTimeKind.Local).AddTicks(9748), "Natus officiis nobis molestiae.", 20, 14, 2 });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "DescriptionTask", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { 2, new DateTime(2019, 7, 12, 10, 19, 20, 571, DateTimeKind.Local).AddTicks(1150), @"Esse recusandae quia velit eos distinctio quasi ut.
Eius eaque est incidunt.", new DateTime(2020, 4, 19, 15, 30, 49, 677, DateTimeKind.Local).AddTicks(6879), "Omnis ab et eum odit qui quasi consequuntur expedita.", 37, 46, 2 });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 1, new DateTime(2019, 7, 11, 20, 1, 35, 538, DateTimeKind.Local).AddTicks(1535), "dolor" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 2, new DateTime(2019, 7, 12, 1, 54, 37, 626, DateTimeKind.Local).AddTicks(8491), "aut" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "TeamId", "TimeOfRegistration" },
                values: new object[] { 1, new DateTime(2002, 4, 28, 7, 50, 58, 371, DateTimeKind.Local).AddTicks(3304), "Westley_Conn@yahoo.com", "Westley", "Conn", 7, new DateTime(2019, 6, 14, 4, 55, 0, 735, DateTimeKind.Local).AddTicks(5536) });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "TeamId", "TimeOfRegistration" },
                values: new object[] { 2, new DateTime(2006, 12, 4, 10, 12, 42, 238, DateTimeKind.Local).AddTicks(7444), "Adolphus_Zulauf@yahoo.com", "Adolphus", "Zulauf", null, new DateTime(2019, 6, 13, 22, 29, 27, 114, DateTimeKind.Local).AddTicks(3526) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Projects");
        }
    }
}
