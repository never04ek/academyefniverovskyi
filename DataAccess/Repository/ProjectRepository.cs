using System.Collections.Generic;
using System.Linq;
using Models;

namespace DataAccess.Repository
{
    public class ProjectRepository : IRepository<Project>
    {
        private AcademyDbContext _dbContext;

        public ProjectRepository(AcademyDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<Project> GetAll()
        {
            return _dbContext.Projects.ToList();
        }

        public void Add(Project entity)
        {
            _dbContext.Projects.Add(entity);
            _dbContext.SaveChanges();
        }

        public void Delete(Project entity)
        {
            _dbContext.Projects.Remove(entity);
            _dbContext.SaveChanges();
        }

        public Project Get(int id)
        {
            return _dbContext.Projects.FirstOrDefault(entity => entity.Id == id);
        }

        public void Update(Project entity)
        {
            _dbContext.Projects.Update(entity);
            _dbContext.SaveChanges();
        }
    }
}