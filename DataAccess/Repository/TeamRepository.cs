using System.Collections.Generic;
using System.Linq;
using Models;

namespace DataAccess.Repository
{
    public class TeamRepository : IRepository<Team>
    {
        private AcademyDbContext _dbContext;


        public TeamRepository(AcademyDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<Team> GetAll()
        {
            return _dbContext.Teams.ToList();
        }

        public void Add(Team entity)
        {
            _dbContext.Teams.Add(entity);
            _dbContext.SaveChanges();
        }

        public void Delete(Team entity)
        {
            _dbContext.Teams.Remove(entity);
            _dbContext.SaveChanges();
        }

        public Team Get(int id)
        {
            return _dbContext.Teams.FirstOrDefault(entity => entity.Id == id);
        }

        public void Update(Team entity)
        {
            _dbContext.Teams.Update(entity);
            _dbContext.SaveChanges();
        }
    }
}