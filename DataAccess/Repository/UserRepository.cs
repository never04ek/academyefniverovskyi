using System.Collections.Generic;
using System.Linq;
using Models;

namespace DataAccess.Repository
{
    public class UserRepository : IRepository<User>
    {
        private AcademyDbContext _dbContext;


        public UserRepository(AcademyDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<User> GetAll()
        {
            return _dbContext.Users.ToList();
        }

        public void Add(User entity)
        {
            _dbContext.Users.Add(entity);
            _dbContext.SaveChanges();
        }

        public void Delete(User entity)
        {
            _dbContext.Users.Remove(entity);
            _dbContext.SaveChanges();
        }

        public User Get(int id)
        {
            return _dbContext.Users.FirstOrDefault(entity => entity.Id == id);
        }

        public void Update(User entity)
        {
            _dbContext.Users.Update(entity);
            _dbContext.SaveChanges();
        }
    }
}