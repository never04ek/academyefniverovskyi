using System.Collections.Generic;
using System.Linq;
using Models;

namespace DataAccess.Repository
{
    public class TasksRepository : IRepository<Tasks>
    {
        private AcademyDbContext _dbContext;


        public TasksRepository(AcademyDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<Tasks> GetAll()
        {
            return _dbContext.Tasks.ToList();
        }

        public void Add(Tasks entity)
        {
            _dbContext.Tasks.Add(entity);
            _dbContext.SaveChanges();
        }

        public void Delete(Tasks entity)
        {
            _dbContext.Tasks.Remove(entity);
            _dbContext.SaveChanges();
        }

        public Tasks Get(int id)
        {
            return _dbContext.Tasks.FirstOrDefault(entity => entity.Id == id);
        }
        
        public void Update(Tasks entity)
        {
            _dbContext.Tasks.Update(entity);
            _dbContext.SaveChanges();
        }
    }
}